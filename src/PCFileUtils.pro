#-------------------------------------------------
#
# Project created by QtCreator 2018-07-28T08:17:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PCFileUtils
TEMPLATE = app


SOURCES += $$PWD/MainWindow.cpp \
        $$PWD/main.cpp\
        $$PWD/operations/impl/FinderImpl.cpp

HEADERS  += $$PWD/MainWindow.h \
    $$PWD/operations/Finder.h \
    $$PWD/operations/impl/FinderImpl.h \
    $$PWD/operations/FindMatcher.h

FORMS    += $$PWD/MainWindow.ui

DISTFILES += \
    PCFileUtils.pri
