#ifndef FINDER_H
#define FINDER_H
#include <QFileInfo>
#include <QList>

#include <operations/FindMatcher.h>
namespace operations
{
    class Finder{
    public:

        virtual void findFiles(const operations::FindMatcher& matcher,
                                                QList<QFileInfo>& output ) = 0;
    };
}
#endif // FINDER_H
