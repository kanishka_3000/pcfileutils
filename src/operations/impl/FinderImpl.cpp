#include "FinderImpl.h"
#include <QDir>
#include <QFileInfoList>
#include <QFileInfo>
operations::FinderImpl::FinderImpl(QString root):
    m_root(root)
{

}

void operations::FinderImpl::findFiles(const operations::FindMatcher &matcher,
                                       QList<QFileInfo> &output)
{

    findFiles(m_root, matcher, output);

}

void operations::FinderImpl::findFiles(QString path,
                                       const operations::FindMatcher &matcher,
                                       QList<QFileInfo> &output)
{
     QDir directory(path);
     QFileInfoList allFiles = directory.entryInfoList();

     for(QFileInfo info : allFiles)
     {
         if(info.isFile())
         {
             if(matcher.match(info))
             {
                 output.push_back(info);
             }
         }
         else
         {
             findFiles(info.absoluteFilePath(), matcher, output);
         }
     }
}
