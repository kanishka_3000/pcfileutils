#ifndef FINDERIMPL_H
#define FINDERIMPL_H

#include <operations/Finder.h>

namespace operations
{
    class FinderImpl : public operations::Finder
    {
    public:
        FinderImpl(QString root);
        void findFiles(const FindMatcher &matcher, QList<QFileInfo> &output) override;

    private:
        void findFiles(QString path, const FindMatcher &matcher, QList<QFileInfo> &output);
        QString m_root;
    };
}
#endif // FINDERIMPL_H
