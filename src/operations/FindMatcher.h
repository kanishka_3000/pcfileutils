#ifndef FINDMATCHER_H
#define FINDMATCHER_H
#include <QFileInfo>

namespace operations
{

    class FindMatcher{
    public:
        virtual bool match(QFileInfo& fileInfo) const= 0;
    };
}
#endif // FINDMATCHER_H
